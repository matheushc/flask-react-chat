import React from 'react'
import './ChatMessage.css'

export default ({ time, name, username, message }) =>
  <div className={name === username ? 'text-you message': name === 'Chatbot' ? 'text-server message' : 'text-others message'}>
    <p>
      <span style={{ fontSize: 12 }}>{time}</span><strong> {name}</strong>
      <br></br>
      <em>{message}</em>
    </p>
  </div>