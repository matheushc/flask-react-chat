import React, { Component } from 'react'
import ChatInput from './ChatInput'
import UsernameInput from './UsernameInput'
import ChatMessage from './ChatMessage'
import io from 'socket.io-client'
import { CONFIG } from './config.js'

const socket = io(CONFIG.SERVER_URL)

class Chat extends Component {
  state = {
    username: '',
    messages: [],
    login: false,
    error: ''
  }

  componentDidMount() {
    socket.on('message', (data) => {
      this.addMessage(data)
    })

    socket.on('login_success', (data) => {
      this.setState({ messages: [] })
      this.setState({ username: data['username'] })
      this.setState({ login: true })
      this.setState({ error: '' })
    })

    socket.on('login_fail', (data) => {
      this.setState({ error: data['message'] })
    })

    socket.on('clients', (data) => {
      if (!data.includes(this.state.username)) {
        this.setState({ login: false })
        this.setState({ messages: [] })
        this.setState({ username: '' })
      }
    })
  }

  addMessage = message => {
    if (message['message'] === '!exit') {
      socket.emit('logout', message)
      socket.emit('get_clients')
    } else {
      this.setState(state => ({ messages: [message, ...state.messages] }))
    }
  }

  submitMessage = messageString => {
    if (messageString.length > 1) {
      const message = { username: this.state.username, message: messageString }
      socket.emit('message', message)
    }
  }

  submitUsername = usernameString => {
    if (usernameString.length > 2 ) {
      socket.emit('login', { username: usernameString })
    } else {
      const error = '"' + usernameString + '" is bad. Username min length is 3.'
      this.setState({ error: error })
    }
  }

  render() {
    return ([
      <div style={!this.state.login ? {} : {display: 'none'}} key='0'> 
        <UsernameInput
          onSubmitUsername={usernameString => this.submitUsername(usernameString)}
          error={this.state.error}
        />
      </div>
      ,
      <div style={this.state.login ? {} : {display: 'none'}} key="1">
        <p>
          Welcome <strong>{ this.state.username }</strong>! | 
          <a href="#top" onClick={() => 
            this.addMessage({username: this.state.username, message: '!exit'})}
            style={{cursor:'pointer', color: 'Blue'}}>Logout</a>
        </p>
        <ChatInput
          onSubmitMessage={messageString => this.submitMessage(messageString)}
        />
        {this.state.messages.map((message, index) =>
          <ChatMessage
            key={index}
            time={message.time}
            name={message.username}
            username={this.state.username}
            message={message.message}
          />,
        )}
      </div>
    ])
  }
}

export default Chat
