import React, { Component } from 'react'
import PropTypes from 'prop-types'

class UsernameInput extends Component {
  static propTypes = {
    onSubmitUsername: PropTypes.func.isRequired,
    error: PropTypes.string.isRequired
  }
  state = {
    username: ''
  }

  render() {
    return (
      <form
        action='.'
        onSubmit={e => {
          e.preventDefault()
          this.props.onSubmitUsername(this.state.username)
          this.setState({ username: '' })
        }}
      >
        <input
          type='text'
          placeholder={'Enter username...'}
          value={this.state.username}
          onChange={e => this.setState({ username: e.target.value })}
          maxLength='25'
        />
        <input type='submit' value={'Login'} />
        <p style={this.props.error ? {} : {display: 'none'}}>
          <strong>Error: {this.props.error}</strong>
        </p>
      </form>
    )
  }
}

export default UsernameInput
