from flask import Flask 
from flask_socketio import SocketIO, emit
from flask_cors import CORS
from datetime import datetime
import json

app = Flask(__name__)
app.config['SECRET_KEY'] = 'mysecret'
socket = SocketIO(app, cors_allowed_origins='*')
CORS(app)

clients = ['Chatbot']

@socket.on('login')
def login(data):
    username = data.get('username')
    if username not in clients:
        emit('login_success', data)
        emit('message', {'time': get_time(), 'username':'Chatbot', 'message':f'{username} joined'}, broadcast=True)
        clients.append(username)
    else:
        emit('login_fail', {'username': 'Chatbot', 'message': 'Username already in use! Try other.'})

@socket.on('logout')
def user_logout(message):
    username = message.get('username')
    if username in clients:
        clients.remove(username)
        emit('message', {'time': get_time(), 'username':'Chatbot', 'message':f'{username} left'}, broadcast=True)

@socket.on('get_clients')
def get_clients():
    emit('clients', clients)

@socket.on('message')
def handleMessage(msg):
    msg['time'] = get_time()
    emit('message', msg, broadcast=True)


def get_time():
    now = datetime.now()
    current_time = now.strftime('%H:%M')
    return current_time
