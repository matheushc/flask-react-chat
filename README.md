# Flask React Chat
## Descrição
Projeto de uma Sala de Chat onde cada usuário pode ingressar com um apelido único para enviar e receber mensagens no grupo.

Projeto desenvolvido em Flask e React.
```
Flask (Servidor) => http://localhost:5000
React (Client) => http://localhost:3000
```
## Clonar o projeto
```
$ git clone git@gitlab.com:matheushc/flask-react-chat.git
```

## Construir o projeto
A construção do projeto é feita com Docker sendo orquestrada por Docker-Compose
```
$ cd flask-react-chat
$ docker-compose up -d --build
```

## Acessar página do Chat
Site em execução: http://localhost:3000


## Stack
```
Python: 3.6
Flask: 1.1.2
NodeJS: 10.22.0
React: 16.13.1
```

## Dependências Python3.6
```
click: 7.1.2
Flask: 1.1.2
Flask-Cors: 3.0.8
Flask-SocketIO: 4.3.1
itsdangerous: 1.1.0
Jinja2: 2.11.2
MarkupSafe: 1.1.1
python-engineio: 3.13.1
python-socketio: 4.6.0
six: 1.15.0
Werkzeug: 1.0.1
```

## Dependências React
```
socket.io-client: 2.3.0
```